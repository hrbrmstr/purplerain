% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/prince.R
\name{prince}
\alias{prince}
\alias{print.prince}
\title{Start a Prince processing chain}
\usage{
prince(path = Sys.which("prince"))

\method{print}{prince}(x, ...)
}
\arguments{
\item{path}{fully qualified path to Prince executable. Will attempt
to find it if no path is specified.}

\item{x}{object}

\item{...}{unused}
}
\value{
prince object
}
\description{
Start a Prince processing chain
}
\examples{
#' if (interactive()) {
prince() \%>\%
  pr_add_sources(
    source_paths = c(
      system.file("examples", "lab-report.html", package = "purplerain")
    )
  ) \%>\%
  pr_add_css_rules("body { font-family: sans-serif; }") \%>\%
  pr_add_css_rules(c(
      "h1, h2.subtitle{ text-align: center; }",
      "h2.subtitle { font-size: 14pt; }"
  )) \%>\%
  pr_add_css_rules("
  #hello {
    color: red;
    text-align: center;
    font-size: large;
    font-style: italic;
    font-family: serif;
  }") \%>\%
  pr_render(tempfile(fileext=".pdf"), open =  TRUE)
}
}
