
[![Project Status: Active – The project has reached a stable, usable
state and is being actively
developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![Signed
by](https://img.shields.io/badge/Keybase-Verified-brightgreen.svg)](https://keybase.io/hrbrmstr)
![Signed commit
%](https://img.shields.io/badge/Signed_Commits-100%25-lightgrey.svg)
[![Linux build
Status](https://travis-ci.org/hrbrmstr/purplerain.svg?branch=master)](https://travis-ci.org/hrbrmstr/purplerain)  
![Minimal R
Version](https://img.shields.io/badge/R%3E%3D-3.6.0-blue.svg)
![License](https://img.shields.io/badge/License-MIT-blue.svg)

# purplerain

Tools to Produce Publication-quality HTML and PDF Output via the Prince
Utility

## Description

Prince (<https://www.princexml.com/>) is a framework and toolchain that
enables the creation of publication-quality HTML and PDF output from XML
or HTML sources. Tools are provided to orchestrate document creation
with Prince.

## WIP

This is package is in rapid development mode. The API will most
certainly change.

## What’s Inside The Tin

The following functions are implemented:

  - `pr_add_css_file`: Add CSS files to be included during processing
  - `pr_add_css_rules`: Add “raw” CSS rules
  - `pr_add_sources`: Add HTML sources to be processed
  - `pr_enable_js`: Enable JavaScript processing
  - `pr_raster`: Render a Prince chain to raster output
  - `pr_render`: Render a Prince chain
  - `pr_set_input_format`: Set input format
  - `pr_set_pdf_metadata`: Set PDF metadata
  - `prince`: Start a Prince processing chain

## Installation

``` r
remotes::install_git("https://git.rud.is/hrbrmstr/purplerain.git")
# or
remotes::install_git("https://git.sr.ht/~hrbrmstr/purplerain")
# or
remotes::install_gitlab("hrbrmstr/purplerain")
# or
remotes::install_bitbucket("hrbrmstr/purplerain")
```

NOTE: To use the ‘remotes’ install options you will need to have the
[{remotes} package](https://github.com/r-lib/remotes) installed.

## Usage

``` r
library(purplerain)

# current version
packageVersion("purplerain")
## [1] '0.1.0'
```

### Render an HTML file to PDF with some extra/custom CSS rules

``` r
prince() %>%
  pr_add_sources(
    source_paths = c(
      system.file("examples", "lab-report.html", package = "purplerain")
    )
  ) %>%
  pr_add_css_rules("body { font-family: sans-serif; }") %>%
  pr_add_css_rules(c(
      "h1, h2.subtitle{ text-align: center; }",
      "h2.subtitle { font-size: 14pt; }"
  )) %>%
  pr_add_css_rules("
  #hello {
    color: red;
    text-align: center;
    font-size: large;
    font-style: italic;
    font-family: serif;
  }") %>%
  pr_render(tempfile(fileext=".pdf"), open =  FALSE)
```

### Rasterize an HTML file to PNG with some extra/custom CSS rules

``` r
prince() %>%
  pr_add_sources(
    source_paths = c(
      system.file("examples", "lab-report.html", package = "purplerain")
    )
  ) %>%
  pr_add_css_rules("body { font-family: sans-serif; }") %>%
  pr_add_css_rules(c(
    "h1, h2.subtitle{ text-align: center; }",
    "h2.subtitle { font-size: 14pt; }"
  )) %>%
  pr_add_css_rules("
  #hello {
    color: red;
    text-align: center;
    font-size: large;
    font-style: italic;
    font-family: serif;
  }") %>%
  pr_raster(tempfile(pattern = "ex-%02d", fileext=".png"))
```

### Render an HTML file from an R Markdown file then render that result to PDF and rasterize it, too

``` r
rmarkdown::render(
  input = system.file("examples", "r-markdown.Rmd", package = "purplerain"),
  output_file = "/tmp/example.html",
  quiet = TRUE
)

prince() %>%
  pr_add_sources("/tmp/example.html") %>%
  pr_render("/tmp/r-markdown.pdf", TRUE)

prince() %>%
  pr_add_sources("/tmp/example.html") %>%
  pr_raster("/tmp/r-markdown-%02d.png")
```

## purplerain Metrics

| Lang | \# Files |  (%) | LoC | (%) | Blank lines |  (%) | \# Lines |  (%) |
| :--- | -------: | ---: | --: | --: | ----------: | ---: | -------: | ---: |
| R    |        8 | 0.89 | 136 | 0.7 |          51 | 0.67 |      148 | 0.78 |
| Rmd  |        1 | 0.11 |  57 | 0.3 |          25 | 0.33 |       41 | 0.22 |

## Code of Conduct

Please note that this project is released with a Contributor Code of
Conduct. By participating in this project you agree to abide by its
terms.
